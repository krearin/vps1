#!/bin/bash

git clone http://github.com/cowrie/cowrie

cd cowrie

python -m venv cowrie-env
source cowrie-env/bin/activate

python -m pip install --upgrade pip
python -m pip install --upgrade -r requirements.txt

cp ../cowrie.cfg etc/cowrie.cfg

bin/cowrie start