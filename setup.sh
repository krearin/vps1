#!/bin/bash

echo "##########################################"
echo "##          Update and Upgrade          ##"
echo "##########################################"

apt-get update -y
apt-get upgrade -y

# fail2ban
echo "##########################################"
echo "##               Fail2Ban               ##"
echo "##########################################"

apt-get install fail2ban -y
cp jail.local /etc/fail2ban/jail.local
systemctl restart fail2ban.service

# ssh
echo "##########################################"
echo "##                 sshd                 ##"
echo "##########################################"
cp sshd_config /etc/ssh/sshd_config
systemctl restart sshd

# cowrie
echo "##########################################"
echo "##                cowrie                ##"
echo "##########################################"
apt-get install git python3-virtualenv libssl-dev libffi-dev build-essential libpython3-dev python3-minimal authbind virtualenv -y
apt-get install python-is-python3 python3-venv -y

adduser --disabled-password --gecos "" cowrie

cp cowrie.sh /home/cowrie/
cp cowrie.cfg /home/cowrie/
chmod 777 /home/cowrie/cowrie.sh
chown cowrie /home/cowrie/cowrie.sh

su -c "/home/cowrie/cowrie.sh" - cowrie

iptables -t nat -A PREROUTING -p tcp --dport 22 -j REDIRECT --to-port 2222
apt-get install iptables-persistent -y
cp cowrie.service /etc/systemd/system/
systemctl enable cowrie
systemctl start cowrie

# openvpn
#echo "##########################################"
#echo "##               openvpn                ##"
#echo "##########################################"
#apt-get install openvpn easy-rsa -y
#cp server.conf /etc/openvpn/
#cp -R pki ../

# wireguard
echo "##########################################"
echo "##              wireguard               ##"
echo "##########################################"

#curl -O https://raw.githubusercontent.com/angristan/wireguard-install/master/wireguard-install.sh
#chmod +x wireguard-install.sh
#echo "DNS Resolver: use 8.8.8.8 and 8.8.4.4"
#./wireguard-install.sh

apt-get install wireguard -y
#wg genkey | dd of=/etc/wireguard/private.key
#chmod go= /etc/wireguard/private.key
#cat /etc/wireguard/private.key | wg pubkey | dd of=/etc/wireguard/public.key

#range_ipv4="10.1.2.0/24"
#range_ipv6_raw=$(printf $(date +%s%N)$(cat /var/lib/dbus/machine-id) | sha1sum | cut -c 31- | cut -c -10)
#range_ipv6="fd$(echo $range_ipv6_raw | cut -c 1-2):$(echo $range_ipv6_raw | cut -c 3-4)$(echo $range_ipv6_raw | cut -c 5-6):$(echo $range_ipv6_raw | cut -c 7-8)$(echo $range_ipv6_raw | cut -c 9-10)::/64"

# a bad idea but anyways (because private and public key are saved):
cp -f wireguard/* /etc/wireguard/

(line=net.ipv4.ip_forward=1; sed -i "/^#$line/ c$line" /etc/sysctl.conf)
(line=net.ipv6.conf.all.forwarding=1; sed -i "/^#$line/ c$line" /etc/sysctl.conf)

sysctl -p
sed -i "s/eth0/$(ip route list default | cut -d " " -f 5)/g" /etc/wireguard/wg0.conf
#ufw is inactive anyways..
ufw allow 51820/udp

systemctl enable wg-quick@wg0.service
systemctl start wg-quick@wg0.service

# TODO: add peers